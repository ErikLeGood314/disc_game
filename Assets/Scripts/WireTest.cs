﻿using UnityEngine;
using System.Collections;

public class WireTest : MonoBehaviour 
{
    public static bool Enabled = false;
	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnPreRender()
    {
        if (Enabled)
            GL.wireframe = true;
    }

    void OnPostRender()
    {
        GL.wireframe = false;
    }
}
