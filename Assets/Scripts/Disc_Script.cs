﻿using UnityEngine;
using System.Collections;

public class Disc_Script : MonoBehaviour 
{
    public static float Speed = 1.5f;
    Transform transformComp;

    bool Rotate = false;
    float Rotate_Speed = 0f;
    public static bool Freeze = false;

	// Use this for initialization
	void Start()
    {
        transformComp = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update()
    {
        if (!Freeze)
        {
            transformComp.Translate(transformComp.forward * Speed * Time.deltaTime, Space.World);
            transformComp.Rotate(transformComp.forward, Rotate_Speed * Time.deltaTime, Space.Self);
        }
	}

    public void Start_Rotate_Mode()
    {
        if (!Rotate)
        {
            Rotate = true;
            Rotate_Speed = Random.Range(-35f, 35f);
        }
    }
    public void Stop_Rotate_Mode()
    {
        if (Rotate)
        {
            Rotate = false;
            Rotate_Speed = 0f;
        }
    }
}
