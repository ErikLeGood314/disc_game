﻿using UnityEngine;
using System.Collections;

public class Pipe_Rave : MonoBehaviour 
{
    Renderer rendererer;
    public Material wireMat;
    public Material otherMat;
    bool wiremat = false;
	// Use this for initialization
	void Start () 
    {
        rendererer = renderer;
	}
	
	// Update is called once per frame
	void Update () 
    {
        //Color col = new Color(Random.Range(0f, 255f), Random.Range(0f, 255f), Random.Range(0f, 255f));
        //rendererer.material.SetColor("WireColor", col);

        if (Input.GetKeyDown(KeyCode.F3))
        {
            if (!wiremat)
            {
                rendererer.material = otherMat;
                wiremat = true;
            }
            else
            {
                rendererer.material = wireMat;
                wiremat = false;
            }
        }
	}
}