﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Pipe_Stitch : MonoBehaviour 
{
    public float range = -240f;

    public GameObject pipe;
    public GameObject pipe2;
    public GameObject pipe3;
    public GameObject pipe4;
    public GameObject pipe5;

    Transform p1;
    Transform p2;
    Transform p3;
    Transform p4;
    Transform p5;
    
    public float speed = 25f;

    float pipe_Length = 0f;

    public static bool RUN = false;

	// Use this for initialization
	void Start () 
    {
        p1 = pipe.transform;
        p2 = pipe2.transform;
        p3 = pipe3.transform;
        p4 = pipe4.transform;
        p5 = pipe5.transform;
        pipe_Length = pipe.GetComponent<BoxCollider>().size.z;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (RUN)
        {
            if (p1.position.z <= transform.position.z - (pipe_Length * 2.5f))
            {
                //p1.position += Vector3.forward * (pipe_Length * 7.5f);
                p1.position = p5.position + (Vector3.forward * (pipe_Length * 4.45f));
            }

            if (p2.position.z <= transform.position.z - (pipe_Length * 2.5f))
            {
                //p2.position += Vector3.forward * (pipe_Length * 7.5f);
                p2.position = p1.position + (Vector3.forward * (pipe_Length * 4.45f));
            }
            if (p3.position.z <= transform.position.z - (pipe_Length * 2.5f))
            {
                //p2.position += Vector3.forward * (pipe_Length * 7.5f);
                p3.position = p2.position + (Vector3.forward * (pipe_Length * 4.45f));
            }
            if (p4.position.z <= transform.position.z - (pipe_Length * 2.5f))
            {
                //p2.position += Vector3.forward * (pipe_Length * 7.5f);
                p4.position = p3.position + (Vector3.forward * (pipe_Length * 4.45f));
            }
            if (p5.position.z <= transform.position.z - (pipe_Length * 2.5f))
            {
                //p2.position += Vector3.forward * (pipe_Length * 7.5f);
                p5.position = p4.position + (Vector3.forward * (pipe_Length * 4.45f));
            }

            float rotSpeed = Disc_Script.Speed * 1.5f;
            speed = Disc_Script.Speed * 10f;

            p1.Translate(-Vector3.forward * speed * Time.deltaTime);
            p1.Rotate(Vector3.forward, rotSpeed * Time.deltaTime, Space.Self);
            p2.Translate(-Vector3.forward * speed * Time.deltaTime);
            p2.Rotate(Vector3.forward, rotSpeed * Time.deltaTime, Space.Self);
            p3.Translate(-Vector3.forward * speed * Time.deltaTime);
            p3.Rotate(Vector3.forward, rotSpeed * Time.deltaTime, Space.Self);
            p4.Translate(-Vector3.forward * speed * Time.deltaTime);
            p4.Rotate(Vector3.forward, rotSpeed * Time.deltaTime, Space.Self);
            p5.Translate(-Vector3.forward * speed * Time.deltaTime);
            p5.Rotate(Vector3.forward, rotSpeed * Time.deltaTime, Space.Self);


        }
	}
}