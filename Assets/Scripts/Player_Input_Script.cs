﻿using UnityEngine;
using System.Collections;

public class Player_Input_Script : MonoBehaviour
{
    public float rotation_Speed = 10f;

    Transform transform_Comp;
    public static Transform public_Transform;
	// Use this for initialization
	void Start () 
    {
        transform_Comp = GetComponent<Transform>();
        public_Transform = transform_Comp;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!Disc_Manager.restart)
        {
            //Placeholder Input (replace with Touchscreen).
            if (Input.GetKey(KeyCode.D))
            {
                transform_Comp.Rotate(-Vector3.forward, rotation_Speed * Time.deltaTime, Space.Self);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform_Comp.Rotate(Vector3.forward, rotation_Speed * Time.deltaTime, Space.Self);
            }

            if (Input.GetKeyDown(KeyCode.F1))
            {
                if (AimBot.Enabled)
                    AimBot.Enabled = false;
                else
                    AimBot.Enabled = true;
            }
            if (Input.GetKeyDown(KeyCode.F2))
            {
                if (WireTest.Enabled)
                    WireTest.Enabled = false;
                else
                    WireTest.Enabled = true;
            }

            if (rigidbody.IsSleeping())
                rigidbody.WakeUp();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //transform_Comp.rotation = Quaternion.Euler(Vector3.zero);
                Disc_Manager.BEGIN = true;
            }
        }
	}

    void OnCollisionEnter()
    {
        transform_Comp.rotation = Quaternion.Euler(Vector3.zero);
        //rigidbody.velocity = Vector3.zero;
        //rigidbody.inertiaTensorRotation = Quaternion.Euler(Vector3.zero);
        Disc_Manager.restart = true;
        Disc_Script.Freeze = true;
        Pipe_Stitch.RUN = false;
    }
}