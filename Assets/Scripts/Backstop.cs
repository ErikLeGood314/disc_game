﻿using UnityEngine;
using System.Collections;

public class Backstop : MonoBehaviour 
{
    Transform transformComp;
    Rigidbody rbody;
	// Use this for initialization
	void Start () 
    {
        transformComp = GetComponent<Transform>();
        rbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (rbody.IsSleeping())
            rbody.WakeUp();
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            other.transform.parent.gameObject.SetActive(false);
            Disc_Manager.pooled_Discs.Add(other.transform.parent.gameObject);
            Disc_Manager.Discs.RemoveAll(s => s == other.transform.parent.gameObject);

            Disc_Manager.Score += 1;
            Disc_Manager.Speed_Up();
        }
    }
}