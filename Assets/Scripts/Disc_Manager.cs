﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Disc_Manager : MonoBehaviour 
{
    public float StartSpeed = 5f;

    public float AccelerationPerHit = 1.5f;
    static float accel;
    public static bool restart = true;

    Transform transform_Comp;

    public static List<GameObject> Discs = new List<GameObject>();

    public static List<GameObject> pooled_Discs = new List<GameObject>();

    public GameObject DiscPrefab;

    GameObject Last_Disc_Spawned;

    public static int Score = 0;
    public static int HIGH_Score = 0;

	// Use this for initialization
	void Start () 
    {
        transform_Comp = GetComponent<Transform>();
        accel = AccelerationPerHit;
        Disc_Script.Speed = StartSpeed;
	}
	
    GameObject Spawn_Disc()
    {
        if (pooled_Discs.Count < 1)
        {
            GameObject newObj = (GameObject)GameObject.Instantiate(DiscPrefab, transform_Comp.position, DiscPrefab.transform.rotation);
            Discs.Add(newObj);
            return Discs[Discs.Count - 1];
        }
        else
        {
            Discs.Add(pooled_Discs[pooled_Discs.Count - 1]);
            pooled_Discs.RemoveAt(pooled_Discs.Count - 1);

            Discs[Discs.Count - 1].transform.position = transform_Comp.position;
            Discs[Discs.Count - 1].transform.rotation = Last_Disc_Spawned.transform.rotation;
            Discs[Discs.Count - 1].SetActive(true);
            return Discs[Discs.Count - 1];
        }
    }

    public static void Speed_Up()
    {
        if (Score > 75)
            Disc_Script.Speed += (accel * 0.1f);
        else
            Disc_Script.Speed += accel;
    }

	// Update is called once per frame
	void Update () 
    {
	    if (!restart)
        {
            if (Discs.Count < 1 && pooled_Discs.Count < 1)
            {
                Last_Disc_Spawned = Spawn_Disc();
            }
            else if (Vector3.Distance(transform_Comp.position, Discs[Discs.Count - 1].transform.position) > 5f)
            {
                //Last_Disc_Spawned.light.enabled = false;

                Last_Disc_Spawned = Spawn_Disc();

                //Last_Disc_Spawned.light.enabled = true;

                Last_Disc_Spawned.transform.Rotate(Vector3.forward, 
                                                   Random.Range(
                                                   (-15f / (Disc_Script.Speed * 0.1f)) + 1f, 
                                                   (15f / (Disc_Script.Speed * 0.1f)) + 1f), 
                                                   Space.Self);
                if (Score > 20)
                {
                    if (Random.Range(0, 1) == 1)
                        Last_Disc_Spawned.GetComponent<Disc_Script>().Start_Rotate_Mode();
                    else
                        Last_Disc_Spawned.GetComponent<Disc_Script>().Stop_Rotate_Mode();
                }
            }
        }
        if (restart)
        {
            if (BEGIN)
            {
                for (int i = 0; i < Discs.Count; i++)
                {
                    GameObject.Destroy(Discs[i]);
                }
                Discs.Clear();
                pooled_Discs.Clear();
                Disc_Script.Speed = StartSpeed;
                Disc_Script.Freeze = false;

                Player_Input_Script.public_Transform.rotation = Quaternion.Euler(Vector3.zero);

                if (Score > HIGH_Score)
                    HIGH_Score = Score;

                Score = 0;
                restart = false;
                BEGIN = false;
                Pipe_Stitch.RUN = true;
            }
        }
	}

    public static bool BEGIN = false;

    Rect hud_Rect = new Rect(5, 1, 285, 35);
    Rect hud_Rect_2 = new Rect(5, 36, 85, 35);
    Rect hud_Rect_3 = new Rect((Screen.width / 2) - 175, Screen.height / 2, 350, 35);

    void OnGUI()
    {
        GUI.Label(hud_Rect, "A, D :- Rotate. Space: Start New.");
        GUI.Label(hud_Rect_2, Score.ToString() + " : " + HIGH_Score.ToString());
        if (restart)
        {
            hud_Rect_3 = new Rect((Screen.width / 2) - 175, Screen.height / 2, 350, 35);
            GUI.Label(hud_Rect_3, "SEIZURE WARNING - FLASHING LIGHTS AND DISCO!");
        }
    }
}